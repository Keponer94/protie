<?php
    include_once("WFramework.php");
    
    
    
    //Return <thead></thead> generated from the query. The <tr> has his own id, this id is equal to h_$id. Each <th> has his own id, this id is equal to $id_theColName
    function createDataTableHeader($query, $id){
        
        $colNames       = getColNames($query);
        $tableHeader    = '<thead><tr id="h_'.$id.'">';
        
        for($i=0;$i<count($colNames);$i++){
         
            $tableHeader = $tableHeader . '<th id="'.$id.'_'.$colNames[$i].'">'.$colNames[$i].'</th>';
            
        }
        
        $tableHeader = $tableHeader . '</tr></thead>';
            
        return $tableHeader;
        
    }

    
//Return <tbody></tbody> generated from the query. Each <tr> has his own id, this id is qual to NumberRow_$id. Each <td> has his own id, this id is equal to b_$id_ColName
//https://www.w3schools.com/jsref/met_tablerow_insertcell.asp
//https://www.w3schools.com/jsref/met_table_insertrow.asp

    function createDataTableBody($query, $id){
        
        $queryArray = queryToArray($query);
        $colNames   = getColNames($query);
        $tableBody  = '<tbody>';
        $numberRow  = 0;
        
        foreach($queryArray as $row){
            
            $tableBody = $tableBody . '<tr id="'.$numberRow.'_'.$id.'">';
            $numberCol = 0;
            
            foreach($row as $field){
                
                $tableBody = $tableBody . '<td id="b_'.$id.'_'.$colNames[$numberRow].'">'.$field.'</td>';
                
            }
            
            $tableBody = $tableBody . '</tr>';
            $numberRow++;
            
        }
        
        $tableBody = $tableBody . '</tbody>';
        
        return $tableBody;
        
    }

    
//Create a full datatable with head and body. See createDataTableHeader and  createDataTableBody to know his ids.
    function createDataTable($query, $id){
        
        $dataTable = createDataTableHeader($query, $id);
        $dataTable = $dataTable . createDataTableBody($query, $id);

        return $dataTable;
        
    }


//Create all the <options> for a select from a query. The id for each <option> is id="s_$id_$option" and the value is the $option
    function createSelectFromQuery($query, $id){
        
        $queryArray = queryToArray($query);
        $select     = "";
        
        foreach($queryArray as $row){
            
            foreach($row as $field){
                
                $select = $select . '<option id="s_'.$id.'_'.$field.'" value="'.$field.'">'.$field.'</option>';
            }
        }
        
        return $select;
        
    }


//Create all the <options> for a select from an array. The id for each <option> is id="s_$id_$option" and the value is the $option
    function createSelectFromArray($array, $id){
        
        $select = "";
        
        foreach($array as $object){
            
            $select = $select . '<option id="s_'.$id.'_'.$object.'" value="'.$object.'">'.$object.'</option>';
            
        }
        
        return $select;
        
    }
    

//Create an input type="$type" with name="i_'.$id.'" id="d_'.$id.'" value=""
    function createInput($id, $type){
        
        $inputDate = '<input type="'.$type.'" name="i_'.$id.'" id="i_'.$id.'" value="'.$id.'" />';
        
        return $inputDate;
    }
    


    
    
    
    ?>
