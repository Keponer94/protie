<?php
    include_once("connect.php");


//Execute a query, if query have been executed return true, else false
    function executeQuery($query){
        
        $result = mysql_query($query);
        
        return $result;
        
    }


//Return the Names of the fields in the query, ordered like the query and begin from 0. format array[number of col]
    function getColNames($query){
        
        $result       = mysql_query($query);
        $colNumber    = mysql_num_fields($result);
        $colNames     = array();
        
        for($i=0;$i<$colNumber;$i++){
            
            $colNames [$i] = mysql_field_name($result, $i);
            
        }
        
        return $colNames;
        
    }


//Return all the data from the query, format array[number of row][name of row] 
    function queryToArray($query){
        
        $colNames       = getColNames($query);
        $queryArray     = array();
        $result         = mysql_query($query);
        
        for($i=0;$row = mysql_fetch_array($result);$i++){
            
            for($j=0;$j<count($colNames);$j++){
                
                $queryArray[$i][$colNames[$j]] = $row[$colNames[$j]];
                
            }
        }
        
        return $queryArray;
        
    }


?>
