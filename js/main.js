function newUser() {

    var nombre = $('#nombreNewUser').val();
    var apellidos = $('#apellidosNewUser').val();
    var mail = $('#mailNewUser').val();
    var pass = $('#passNewUser').val();
    var pais = $('#paisNewUser').val();
    var codTipo = $('#codTipoNewUser').val();
    var codPermisos = $('#codPermisosNewUser').val();
    var codPuntuacion = $('#codPuntuacionNewUser').val();


    $.post('proc.php', {
        f: 1,
        nombre: nombre,
        apellidos: apellidos,
        mail: mail,
        pass: pass,
        pais: pais,
        codTipo: codTipo,
        codPermisos: codPermisos,
        codPuntuacion: codPuntuacion
    }, function (rtn) {
        alert(rtn);
    });

}


//Call newMailSubscription in funciones and show modalCorrectSubscription if mail have been inserted or modalinCorrectSubscription if not
function newMailSubscription() {

    var mail = $('#mailSubscrition').val();

    $.post('proc.php', {

        f: 2,
        mailSubscription: mail

    }, function (rtn) {

        if (rtn) {

            $('#modalCorrectSubscription').modal({
                show: false
            })
            $('#modalCorrectSubscription').modal('show');

        } else {

            $('#modalIncorrectSubscription').modal({
                show: false
            })
            $('#modalIncorrectSubscription').modal('show');

        }

    });

}

//Call newSuggestion in funciones and show modalCorrectSuggestion if suggestion have been inserted or modalinCorrectSuggestion if not
function newSuggestion() {

    var suggestion = $('#textSuggestion').val();

    $.post('proc.php', {

        f: 3,
        suggestion: suggestion

    }, function (rtn) {

        if (rtn) {

            $('#modalCorrectSuggestion').modal({
                show: false
            })
            $('#modalCorrectSuggestion').modal('show');

        } else {

            $('#modalIncorrectSuggestion').modal({
                show: false
            })
            $('#modalIncorrectSuggestion').modal('show');

        }

    });

}


//Call selectArea and return HTML code
function selectArea() {

    $.post('proc.php', {

        f: 4,

    }, function (rtn) {

        $("#selectArea").html('<option value="0" selected >Seleccione una opción</option>' + rtn);

    });

}


//Call selectTipoEspecialista and return HTML code
function selectTipoEspecialista() {

    $.post('proc.php', {

        f: 5,
        area: $("#selectArea").val()

    }, function (rtn) {

        $("#selectTipoEspecialista").html(rtn);

    });

}

function sendContactoProfesionales() {

    var selectedArea = $("#selectArea").val();
    var selectedTipoEspecilista = $("#selectTipoEspecialista").val();
    var mail = $('#mailSubscrition').val();

    $.post('proc.php', {

        f: 6,
        area: selectedArea,
        tipoEspecialista: selectedTipoEspecilista,
        mail: mail

    }, function (rtn) {

        if (rtn) {

            $('#modalCorrectSubscription').modal({
                show: false
            })
            $('#modalCorrectSubscription').modal('show');

        } else {

            $('#modalIncorrectSubscription').modal({
                show: false
            })
            $('#modalIncorrectSubscription').modal('show');

        }

    });



}
