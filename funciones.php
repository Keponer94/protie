<?php    

include_once("WFrameworkHTML.php");



function newUser($nombre, $apellidos, $mail, $pass, $pais, $codTipo, $codPermisos, $codPuntuacion){
    
    $sql = "INSERT INTO Usuarios (idUsuario, nombre, apellidos, mail, pass, pais, codTipo, codPermisos, codPuntuacion)
	 		VALUES ('{$nombre}', '{$apellidos}', '{$mail}', '{$pass}', '{$pais}', {$codTipo}, {$codPermisos}, {$codPuntuacion})";
    
	return executeQuery($sql);
    }


function newMailSubscription($mailSubscription){
    
    $sql = "INSERT INTO SubcripcionMail (mail) VALUES ('{$mailSubscription}')";
    
    return executeQuery($sql);

}


function selectArea() {
    
    return createSelectFromQuery("SELECT `Area` FROM `Area`", "area");
    
}


function selectTipoEspecialista($area){
    
    $idArea = queryToArray("SELECT `idArea` FROM `Area` WHERE `Area` = '{$area}'");
    
    return createSelectFromQuery("SELECT `tipoEspecialista` FROM `TipoEspecialista` WHERE idArea = {$idArea[0]['idArea']} ");
    
}


function sendContactoProfesionales($area, $tipoEspecialista, $mail){
    
    $sql = "INSERT INTO SubscripcionProfesionales (area, tipoEspecialista, mail) VALUES ('{$area}', '{$tipoEspecialista}', '{$mail}')";
    
    return executeQuery($sql);

}



/*
function newMailSubscription($suggestion){
    $sql = "INSERT INTO Sugerencia (sugerencia) VALUES ('{$suggestion}')";
    
    return executeQuery($sql);
}

*/

?>
